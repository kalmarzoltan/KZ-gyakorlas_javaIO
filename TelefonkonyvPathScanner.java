package java_IO.gyakorlas;

import java.util.InputMismatchException;
import java.util.Scanner;

public class TelefonkonyvPathScanner {

    public String scanTheTelefonKonyvPath() {
        String path = null;

        try {
            System.out.println("Írd be a telefonkönyv elérési útvonalat.");
            Scanner sc = new Scanner(System.in);
            path = sc.nextLine();

            System.out.println("A megadott elérési út a: "+path);
            sc.close();

        } catch (InputMismatchException ex) {
            System.out.println("InputMismatchException.");
        }
        return path;

    }


}
