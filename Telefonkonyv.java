package java_IO.gyakorlas;

import java.util.Map;

public class Telefonkonyv {

    private Map telefonKonyv;

    public Telefonkonyv() {
        TelefonkonyvPathScanner tps = new TelefonkonyvPathScanner();
        TelefonKonyvReader tr = new TelefonKonyvReader();
        this.telefonKonyv = tr.readTelefonKonyv(tps.scanTheTelefonKonyvPath());
    }

    public Map getTelefonKonyv() {
        return telefonKonyv;
    }

}
