``` java
 BufferedReader
BufferedReader br = new BufferedReader(new FileReader("text.txt"));
String sor;
while ((sor = br.readLine()) != null) {
System.out.println(sor);
}
br.close();
```


<h4> Feladat:<h4>

Készítsünk egy telefonkönyvet:
- Hozzunk létre egy telefonkönyv osztályt
- Legyen egy HashMap adattgja, amiben a nevek a kulcsok és a telefonszámok
az értékek
- Ezt a map-et egy fájl-ból töltsük fel adatokkal, méghozzá úgy, hogy a
Telefonkonyv osztály konstruktorában megadható legyen a fájl.
- Ezután irassuk ki a teljes telefonkönyv tartalmát a main metódusban

<h4> Output: <h4>


>Írd be a telefonkönyv elérési útvonalat.
>C:\Users\Kalmi\Documents\telefonkonyv.txt <br/>
>A megadott elérési út a: C:\Users\Kalmi\Documents\telefonkonyv.txt<br/>
> {Charles Bronson=222-222, Chuck Norris=111-111, Telly Savalas=333-333}
>
>Process finished with exit code 0
