package java_IO.gyakorlas;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class TelefonKonyvReader {


    public HashMap readTelefonKonyv(String path) {
        Map telefonKonyv = new HashMap<>();
        String line;
        BufferedReader bfr = null;
        try {
            bfr = new BufferedReader(
                    new FileReader(path));

            while ((line = bfr.readLine()) != null) {
                //	System.out.println(line);
                telefonKonyv.put(line.toString().split(";")[0], line.toString().split(";")[1]);
                // bfr.close(); IDE EZ NEM KELL!!!
            }

        } catch (FileNotFoundException e) {
            System.out.println("File not found exception van...");
        } catch (IOException e) {
            System.out.println("IO exception van...");
        } finally {
            try {
                bfr.close();
            } catch (IOException e) {
                System.err.println("Nem lehetett lezárni a file-t");
            }
        }

        return (HashMap) telefonKonyv;

    }


}
